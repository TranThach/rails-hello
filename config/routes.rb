Rails.application.routes.draw do
  get 'users/new'
  root 'web/static_pages#home'
  get 'static_pages/home', to: 'web/static_pages#home', as: 'ruby_home'
  get 'static_pages/help', controller: 'web/static_pages', action: 'help'
  get 'static_pages/about', controller: 'web/static_pages', action: 'about'
  get 'static_pages/contact', controller: 'web/static_pages', action: 'contact'
  get 'signup', to: 'web/users#new'
  
  resources :microposts, controller: 'web/microposts'
  resources :users, controller: 'web/users'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
